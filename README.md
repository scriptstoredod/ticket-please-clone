<div dir="ltr" style="text-align: left;" trbidi="on">
<span style="background-color: white; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px;">A popular Movie Booking Script for you consisting of Counter Booking, Ticket Management System, Ticket Availability option. Our Ticket Please Clone Script has all the relevant features such as Seat Selection, Advance CMS Management, Print and Cancel ticket through online, Location management etc… which would result in bringing a hike to your business career. Every business wish to be one or other way the same and if you want to start a Movie Booking website, this will be the best way you have chosen.</span><br />
<span style="background-color: white; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px;"><br /></span>
<h2 style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 17px; line-height: 1.1; margin: 0px 0px 20px; text-transform: uppercase;">
PRODUCT DESCRIPTION</h2>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">Unique Features:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">&nbsp;Login</em></strong><ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Log in using a valid username or email and password</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Recover the username/password using forgot password</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Recover login details using your registered mail id</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Registration</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Create a new account with basic information</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Username , email and password and its</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Email validation and captcha is available</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Confirmation mail will be sent to the registered mail id</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Check availability</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">After login user can search movie from movie list.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">User can check ticket availability.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">If a ticket is available user can book ticket.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Shows the trailers by using YouTube video.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">The user can rate the films</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Book ticket</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">User can search Movie by using movie, date, show, location, language and class.&nbsp;</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Then select number of seats to book.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">After select number of seats, select seat numbers from seat layout.</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
</li>
</ul>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">Advanced Features:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Movie, Cinema and Events Booking</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Counter booking and online booking</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Advance Theatre management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Class management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Show management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Movie management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Events management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Location management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">News management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Schedule management</em></li>
</ul>
</li>
</ul>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Admin panel:</em></strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Login:</em></strong><ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Login using valid username and password</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Change password:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Password management (Change new password)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">User management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage user(view and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Theater management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Add the new theater</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage theater(view, edit and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Class management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Add the new class</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage class (view, edit and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Show management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Add the new show</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage show (view, edit and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Movie management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can add new movie.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can edit old movie details.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can delete movie.</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Events management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can add new events.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can edit old events details.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can delete events.</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Location management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Add the new location</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage location (view, edit and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">News management:&nbsp;</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Add the new news</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Manage news (view, edit and delete)</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Schedule management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can add new movie schedule by upcoming and current.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can edit movie schedule.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can delete movie schedule.</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Report management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can view daily report, weekly report, monthly report.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can view counter user report.</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">Site settings:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">You can manage site settings and information’s</em></li>
</ul>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<br /></div>
<div style="box-sizing: border-box; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">CMS management:</em></strong></div>
<ul style="box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 18px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin can manage the cms pages</em></li>
</ul>
</li>
</ul>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">WALLET USER:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Personal my account details with history.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Reports</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Get Email Options.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Get SMS Options.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Search option enabled</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">User Wallet available.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Payment Gateway enabled.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Fund transfer.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Check refund status.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Coupon codes.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Promotions codes.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Wallet offers credits.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Refer a friends…etc</em></li>
</ul>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;"><em style="box-sizing: border-box;">&nbsp;</em>GUEST USER:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Search option enabled.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Can view en-numbers of bus snaps and videos.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Fill details and book it.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Payment Gateway enabled.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Check refund status.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Coupon codes.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Promotions codes.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Wallet offers credits.</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Refer a friends…etc</em></li>
</ul>
<div>
<span style="font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><br /></i></span></span></div>
<div>
<span style="font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><br /></i></span></span></div>
<div>
<span style="font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><b>Check out websites:</b></i></span></span></div>
<div>
<span id="docs-internal-guid-e15b10c5-c5f1-252b-1c13-3a36685b1c28"><div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<br /></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="https://www.doditsolutions.com/">https://www.doditsolutions.com/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<br /></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="http://scriptstore.in/">http://scriptstore.in/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<br /></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="http://phpreadymadescripts.com/">http://phpreadymadescripts.com/</a></span></div>
<div>
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><br /></span></div>
</span></div>
</div>
